
class Empleado():
	def __init__(self, nombre, nomina):
		self.nombre = nombre
		self.nomina = nomina
	
	
	def CalculoImpuestos (self, nombre, nom):
		impuestos = self.nomina *0.30
		print ("El empleado {name} debe pagar {tax:.2f}".format(name=nombre,
		tax=impuestos))
		return impuestos
		
def displayCost(total):
	print("Los impuestos a pagar en total son {:.2f} euros".format(total))
	
empleadoPepe = Empleado('Pepe', 20000)
empleadoAna = Empleado('Ana, 30000)

displayCost(empleadoPepe.CalculoImpuestos() + empleadoAna.CalculoImpuestos())
